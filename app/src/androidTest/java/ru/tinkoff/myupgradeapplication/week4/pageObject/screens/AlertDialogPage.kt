package ru.tinkoff.myupgradeapplication.week4.pageObject.screens

import androidx.test.uiautomator.By
import androidx.test.uiautomator.BySelector
import ru.tinkoff.myupgradeapplication.week4.pageObject.assertions.AlertDialogPageAssertions

/**
 * @author i.gatin
 */
class AlertDialogPage : BasePage(), AlertDialogPageAssertions {
    val alertDialogTitle: BySelector = By.text(ALERT_DIALOG_TITLE_TEXT)
    val alertDialogText: BySelector = By.text(ALERT_DIALOG_TEXT)

    override fun getAlertTitle(): BySelector {
        return alertDialogTitle
    }

    override fun getAlertText(): BySelector {
        return alertDialogTitle
    }

    private companion object {
        const val ALERT_DIALOG_TITLE_TEXT = "Важное сообщение"
        const val ALERT_DIALOG_TEXT = "Теперь ты автоматизатор"
    }
}
