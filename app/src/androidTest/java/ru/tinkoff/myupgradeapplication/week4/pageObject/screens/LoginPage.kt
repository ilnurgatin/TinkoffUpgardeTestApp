package ru.tinkoff.myupgradeapplication.week4.pageObject.screens

import androidx.test.uiautomator.By
import androidx.test.uiautomator.BySelector
import androidx.test.uiautomator.Until
import ru.tinkoff.myupgradeapplication.week4.pageObject.assertions.LoginPageAssertions

class LoginPage : BasePage(), LoginPageAssertions {
    val loginFiledSelector: BySelector = By.res("ru.tinkoff.myupgradeapplication:id/edittext_login")
    val passwordFiledSelector: BySelector = By.res("ru.tinkoff.myupgradeapplication:id/edittext_password")

    private val submitButtonSelector = By.res("ru.tinkoff.myupgradeapplication:id/button_submit")
    private val previousButtonSelector = By.res("ru.tinkoff.myupgradeapplication:id/button_second")

    fun clickPreviousButton() {
        device
            .wait(Until.findObject(previousButtonSelector), waitingTimeOut)
            .click()
    }

    fun setLogin(loginValue: String) {
        device
            .wait(Until.findObject(loginFiledSelector), waitingTimeOut)
            .text = loginValue
    }

    fun setPassword(passwordValue: String) {
        device
            .wait(Until.findObject(passwordFiledSelector), waitingTimeOut)
            .text = passwordValue
    }

    fun clickSubmitButton() {
        device
            .wait(Until.findObject(submitButtonSelector), waitingTimeOut)
            .click()
    }
}
