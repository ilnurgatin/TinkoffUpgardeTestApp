package ru.tinkoff.myupgradeapplication.week4

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Rule
import org.junit.Test
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.R
import ru.tinkoff.myupgradeapplication.week4.pageObject.screens.MainPage
import ru.tinkoff.myupgradeapplication.week4.pageObject.screens.LoginPage

/**
 * @author i.gatin
 */
class NavigationTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    private val mainPage = MainPage()
    private val loginPage = LoginPage()

    @Test
    fun defaultTextAfterNavigationTest() {
        with(mainPage) {
            checkTextViewContent(defaultTextSample)
            clickChangeButton()
            checkTextViewContent(defaultTextSampleAfterChangeButtonClick)
            clickNextButton()
        }
        with(loginPage) {
            clickPreviousButton()
        }
        with(mainPage) {
            checkTextViewContent(defaultTextSample)
        }
    }

    @Test
    fun emptyFieldsAfterNavigationTest() {
        with(mainPage) {
            clickNextButton()
        }
        with(loginPage) {
            setLogin(LOGIN_VALUE)
            setPassword(PASSWORD_VALUE)
            clickPreviousButton()
        }
        with(mainPage) {
            clickNextButton()
        }
        with(loginPage) {
            checkEmptyPasswordField()
            checkEmptyLoginField()
        }
    }

    private companion object {
        const val LOGIN_VALUE = "Tinkoff"
        const val PASSWORD_VALUE = "Upgrade"
        val defaultTextSample = InstrumentationRegistry.getInstrumentation().targetContext.getString(
            R.string.first_text)
        val defaultTextSampleAfterChangeButtonClick = InstrumentationRegistry.getInstrumentation().targetContext.getString(
            R.string.second_text)
    }
}
