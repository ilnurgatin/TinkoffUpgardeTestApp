package ru.tinkoff.myupgradeapplication.week4

import androidx.test.espresso.action.ViewActions.pressBack
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Rule
import org.junit.Test
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.R
import ru.tinkoff.myupgradeapplication.week4.pageObject.screens.AlertDialogPage
import ru.tinkoff.myupgradeapplication.week4.pageObject.screens.MainPage

/**
 * @author i.gatin
 */
class DialogWindowTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    private val mainPage = MainPage()
    private val alertDialogPage = AlertDialogPage()

    @Test
    fun checkTitleAndTextAlertWindowTest() {
        with(mainPage) {
            clickShowDialogButton()
        }
        with(alertDialogPage) {
            checkTitle()
            checkText()
        }
    }

    @Test
    fun closeDialogWindowTest() {
        with(mainPage) {
            clickShowDialogButton()
        }
        with(alertDialogPage) {
            checkTitle()
            pressBack()
        }
        with(mainPage) {
            checkTextOnScreen(showDialogButtonName)
        }
    }

    private companion object {
        val showDialogButtonName = InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.show_dialog)
    }
}
