package ru.tinkoff.myupgradeapplication.week4.pageObject.assertions

import androidx.test.uiautomator.BySelector
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import ru.tinkoff.myupgradeapplication.week4.pageObject.screens.AlertDialogPage

/**
 * @author i.gatin
 */
interface AlertDialogPageAssertions {
    val device: UiDevice
    val waitingTimeOut: Long

    fun checkTitle() {
        assert(device.wait(Until.hasObject(getAlertTitle()), waitingTimeOut))
    }

    fun checkText() {
        assert(device.wait(Until.hasObject(getAlertText()), waitingTimeOut))
    }

    fun getAlertTitle(): BySelector {
        return AlertDialogPage().alertDialogTitle
    }

    fun getAlertText(): BySelector {
        return AlertDialogPage().alertDialogText
    }
}
