package ru.tinkoff.myupgradeapplication.week4.pageObject.screens

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice

open class BasePage {
    val device: UiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    val waitingTimeOut = 2000L

    fun pressBackButton() {
        device.pressBack()
    }
    fun getWaitingTimeOut(): Long {
        return waitingTimeOut
    }
    fun getDevice(): UiDevice {
        return device
    }
}
