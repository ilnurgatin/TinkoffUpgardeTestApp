package ru.tinkoff.myupgradeapplication.week4.pageObject.assertions

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import ru.tinkoff.myupgradeapplication.R
import ru.tinkoff.myupgradeapplication.week4.pageObject.screens.LoginPage

/**
 * @author i.gatin
 */
interface LoginPageAssertions {
    fun getDevice(): UiDevice

    fun getWaitingTimeOut(): Long

    fun checkTextOnSnackBar(text: String) {
        assert(getDevice().wait(Until.hasObject(By.text(text)), getWaitingTimeOut()))
    }

    fun checkEmptyLoginField() {
        assert(getDevice()
            .wait(Until.findObject(LoginPage().loginFiledSelector), getWaitingTimeOut())
            .text == LOGIN_FIELD_HINT_TEXT)
    }

    fun checkEmptyPasswordField() {
        assert(getDevice()
            .wait(Until.findObject(LoginPage().passwordFiledSelector), getWaitingTimeOut())
            .text == PASSWORD_FIELD_HINT_TEXT)
    }

    private companion object {
        val LOGIN_FIELD_HINT_TEXT = InstrumentationRegistry.getInstrumentation().targetContext.getString(
            R.string.login_hint)
        val PASSWORD_FIELD_HINT_TEXT = InstrumentationRegistry.getInstrumentation().targetContext.getString(
            R.string.password_hint)

    }

}
