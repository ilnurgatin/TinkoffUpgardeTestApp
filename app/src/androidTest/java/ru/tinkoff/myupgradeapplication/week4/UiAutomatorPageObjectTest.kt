package ru.tinkoff.myupgradeapplication.week4

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Rule
import org.junit.Test
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.week4.pageObject.screens.MainPage
import ru.tinkoff.myupgradeapplication.week4.pageObject.screens.LoginPage
import ru.tinkoff.myupgradeapplication.R

class UiAutomatorPageObjectTest {
    //Это тест образец из самого задания. Пришлось подправить его тоже т.к использует те же пейджи что и я, иначе руинил сборку
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun enterLoginPasswordTest(){
        val loginValue = "Tinkoff"
        val passwordValue = "Upgrade"
        with(MainPage()){
            clickNextButton()
        }
        with(LoginPage()){
            setLogin(loginValue)
            setPassword(passwordValue)
            clickSubmitButton()
            checkTextOnSnackBar("You enter login = $loginValue password = $passwordValue")
        }
    }

    @Test
    fun checkSwitchingTextTest(){

        val firstText = InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.first_text)
        val secondText = "qwertyuio" //InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.second_text)

        with(MainPage()){
            checkTextOnScreen(firstText)
            clickChangeButton()
            checkTextOnScreen(secondText)
        }
    }

}
