package ru.tinkoff.myupgradeapplication.week4

import androidx.test.ext.junit.rules.ActivityScenarioRule
import org.junit.Rule
import org.junit.Test
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.week4.pageObject.screens.MainPage
import ru.tinkoff.myupgradeapplication.week4.pageObject.screens.LoginPage

/**
 * @author i.gatin
 */
class LoginPageNegativeTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    private val mainPage = MainPage()
    private val loginPage = LoginPage()

    @Test
    fun emptyPasswordFieldTest() {
        with(mainPage) {
            clickNextButton()
        }
        with(loginPage){
            setLogin(LOGIN_VALUE)
            clickSubmitButton()
            checkTextOnSnackBar(EMPTY_PASSWORD_SNACK_MESSAGE)
        }
    }

    @Test
    fun emptyLoginFieldTest() {
        with(mainPage) {
            clickNextButton()
        }
        with(loginPage){
            setPassword(PASSWORD_VALUE)
            clickSubmitButton()
            checkTextOnSnackBar(EMPTY_LOGIN_SNACK_MESSAGE)
        }
    }

    @Test
    fun emptyLoginAndPasswordFieldsTest() {
        with(mainPage) {
            clickNextButton()
        }
        with(loginPage){
            clickSubmitButton()
            checkTextOnSnackBar(EMPTY_LOGIN_AND_PASSWORD_SNACK_MESSAGE)
        }
    }

    private companion object {
        const val LOGIN_VALUE = "Tinkoff"
        const val PASSWORD_VALUE = "Upgrade"
        const val EMPTY_PASSWORD_SNACK_MESSAGE = "Password field must be filled!"
        const val EMPTY_LOGIN_SNACK_MESSAGE = "Login field must be filled!"
        const val EMPTY_LOGIN_AND_PASSWORD_SNACK_MESSAGE = "Both of fields must be filled!"
    }
}
