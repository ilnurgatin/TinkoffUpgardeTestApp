package ru.tinkoff.myupgradeapplication.week4.pageObject.screens

import androidx.test.uiautomator.By
import androidx.test.uiautomator.BySelector
import androidx.test.uiautomator.Until
import ru.tinkoff.myupgradeapplication.week4.pageObject.assertions.MainPageAssertions

class MainPage : BasePage(), MainPageAssertions{

    private val nextButtonSelector = By.res("ru.tinkoff.myupgradeapplication:id/button_first")
    private val changeButtonSelector = By.res("ru.tinkoff.myupgradeapplication:id/change_button")
    private val showDialogButton = By.res("ru.tinkoff.myupgradeapplication:id/dialog_button")

    val textView: BySelector = By.res("ru.tinkoff.myupgradeapplication:id/textview_first")

    fun clickNextButton() {
        device
            .wait(Until.findObject(nextButtonSelector), waitingTimeOut)
            .click()
    }

    fun clickChangeButton() {
        device
            .wait(Until.findObject(changeButtonSelector), waitingTimeOut)
            .click()
    }

    fun clickShowDialogButton() {
        device
            .wait(Until.findObject(showDialogButton), waitingTimeOut)
            .click()
    }
}
