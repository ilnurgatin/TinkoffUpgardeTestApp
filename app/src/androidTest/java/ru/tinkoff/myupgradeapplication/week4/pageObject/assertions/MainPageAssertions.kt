package ru.tinkoff.myupgradeapplication.week4.pageObject.assertions

import androidx.test.uiautomator.By
import androidx.test.uiautomator.BySelector
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import ru.tinkoff.myupgradeapplication.week4.pageObject.screens.MainPage

/**
 * @author i.gatin
 */
interface MainPageAssertions {
    fun getDevice(): UiDevice

    fun getWaitingTimeOut(): Long

    fun checkTextOnScreen(firstText: String) {
        assert(
            getDevice()
                .wait(Until.hasObject(By.text(firstText)), getWaitingTimeOut())
        )
    }

    fun checkTextViewContent(text: String) {
        assert(
            getDevice().wait(Until.findObject(getSelectorByText()), getWaitingTimeOut()).text == text
        )
    }

    fun  getSelectorByText(): BySelector {
        return MainPage().textView
    }
}
